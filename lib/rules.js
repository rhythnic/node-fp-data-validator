const { Success, Failure } = require('folktale/validation')

const rule = (id, fn, params) => async val => {
  try {
    await fn(params, val)
    return Success(val)
  } catch (err) {
    return Failure([{
      message: err.message,
      rule: id,
      params,
      value: val,
      path: []
    }])
  }
}

const exists = val => val != null && val !== ''

const checkExists = (_, val) => {
  if (!exists(val)) throw new Error('Required')
}

const checkSomeExist = ({ props, min = 1 }, val) => {
  const count = props.reduce((acc, x) => acc + (exists(val[x]) ? 1 : 0), 0)
  if (count < min) {
    throw new TypeError(`At least ${min} of the following properties must be included: ${props.join(', ')}`)
  }
}

const checkType = ({ type }, val) => {
  if (typeof val !== type) throw new TypeError(`Must be a ${type}`)
}

const checkArray = (_, val) => {
  if (!Array.isArray(val)) throw new TypeError('Must be an array')
}

const checkLength = ({ length }, val) => {
  if (val.length !== length) throw new Error(`Must have length of ${length}`)
}

const checkMinLength = ({ length }, val) => {
  if (val.length < length) throw new Error(`Must have length of ${length} or more`)
}

const checkEquals = ({ value }, val) => {
  if (val !== value) throw new Error(`Must be ${value}`)
}

const checkEmail = (_, val) => {
  const emailRegex = /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/
  if (!emailRegex.test(val)) {
    throw new Error('Invalid email address')
  }
}

const checkPattern = ({ pattern }, val) => {
  if (!(new RegExp(pattern).test(val))) {
    throw new Error(`Must match this pattern: ${pattern}`)
  }
}

const required = rule('required', checkExists)
const requireOne = props => rule('require-one', checkSomeExist, { props })
const is = type => rule(`is-${type}`, checkType, { type })
const isArray = rule('is-array', checkArray)
const hasLength = length => rule('hasLength', checkLength, { length })
const minLength = length => rule('minLength', checkMinLength, { length })
const equals = value => rule('equals', checkEquals, { value })
const email = rule('email', checkEmail)
const hasPattern = pattern => rule('has-pattern', checkPattern, { pattern })

module.exports = {
  rule,
  required,
  requireOne,
  is,
  isArray,
  hasLength,
  minLength,
  equals,
  email,
  hasPattern
}
