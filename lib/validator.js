const { Success, Failure } = require('folktale/validation')
const { required: requiredRule, isArray } = require('./rules')

const adjustPath = pathItem => data => ({
  ...data,
  path: [pathItem, ...data.path]
})

const map = fn => xs => xs.map(fn)

const validator = rules => value => rules
  .reduce(
    (acc, assertion) => acc.concat(assertion(data)),
    Success(value)
  )
  .map(_ => value)

const prop = required => (prop, rules) => {
  if (Array.isArray(rules)) rules = validator(rules)
  const mapFailure = adjustPath(prop)
  const runRules = propVal => rules(propVal).mapFailure(map(mapFailure))
  return val => requiredRule(val[prop]).matchWith({
    Success: propVal => runRules(propVal).concat(Success(val)),
    Failure: failureVal => required ? Failure(mapFailure(failureVal)) : Success(val)
  })
}

const item = rules => {
  if (Array.isArray(rules)) rules = validator(rules)
  const runRules = val => val
    .reduce(
      (acc, _item, i) => acc.concat(rules(_item).mapFailure(map(mapFailure(i)))),
      Success()
    )
    .concat(Success(val))
  return val => isArray(val).matchWith({
    Success: () => runRules(val).concat(Success(val)),
    Failure
  })
}

const required = prop(true)
const optional = prop(false)

module.exports = {
  validator,
  prop,
  item,
  required,
  optional
}
