const { Success, Failure } = require('folktale/validation')

const {
  optional,
  items,
  isArray,
  isType,
  minLength,
  hasLength,
  equals,
  isString,
  isBoolean
} = require('./validators')

describe('validators unit tests', () => {
  test('optional', () => {
    let validator = optional(isString)[0]('', 'a')
    expect(Success.hasInstance(validator({}))).toBe(true)
    expect(Success.hasInstance(validator({ a: 'a' }))).toBe(true)
    expect(Failure.hasInstance(validator({ a: 1 }))).toBe(true)
  })

  test('isType', () => {
    let validator = isType('string')('a', 'b')
    expect(Success.hasInstance(validator({ b: 'a' }))).toBe(true)
    expect(Failure.hasInstance(validator({ b: 0 }))).toBe(true)
  })
})
