const { validator, required, optional } = require('../lib/validator')
const {
  is,
  minLength,
  hasLength,
  equals,
  requireOne,
  email } = require('../lib/rules')

const Reference = type => validator([
  required('id', [is(String), minLength(2)]),
  required('type', [equals(type)])
])

const validateUser = validator([
  requireOne(['username', 'email']),
  required('pwdHash', [is('string'), hasLength(16)]),
  required('company', Reference('Company')),
  optional('username', [is('string'), minLength(2)]),
  optional('email', [email]),
  optional('friends', [
    item(Reference('User'))
  ])
])

module.exports = {
  validateUser
}
